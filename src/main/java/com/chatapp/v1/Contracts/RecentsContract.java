package com.chatapp.v1.Contracts;

import android.content.Context;

import com.chatapp.v1.Adapter.RecentsListAdapter;
import com.chatapp.v1.Base.BaseView;
import com.cometchat.pro.models.BaseMessage;
import com.cometchat.pro.models.Conversation;
import com.chatapp.v1.Base.BasePresenter;

import java.util.List;


public interface RecentsContract {

    interface RecentsView extends BaseView {

        void setRecentAdapter(List<Conversation> conversationList);

        void updateUnreadCount(Conversation conversation);

        void setLastMessage(Conversation conversation);

        void setFilterList(List<Conversation> hashMap);

        void refreshConversation(BaseMessage message);

        void setDeleteConversation(Conversation conversation);
    }

    interface RecentsPresenter extends BasePresenter<RecentsView> {

          void fetchConversations(Context context);

          void addMessageListener(String presenceListener);

          void removeMessageListener(String string);

//          void searchConversation(String s);

          void updateConversation();

          //void deleteConversation(Conversation conversation);4
        void deleteConversation(Context context, String conversationID, RecentsListAdapter recentsListAdapter);
    }
}
