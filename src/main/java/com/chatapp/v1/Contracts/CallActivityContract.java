package com.chatapp.v1.Contracts;

import android.content.Context;

import com.chatapp.v1.Base.BasePresenter;

public interface CallActivityContract {

    interface CallActivityView{

    }

    interface CallActivityPresenter extends BasePresenter<CallActivityView>{

        void removeCallListener(String listener);

        void addCallListener(Context context,String listener);

    }
}
