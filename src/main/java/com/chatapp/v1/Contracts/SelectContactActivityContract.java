package com.chatapp.v1.Contracts;

import android.content.Intent;

import com.chatapp.v1.Activity.SelectContactActivity;
import com.chatapp.v1.Base.BasePresenter;
import com.cometchat.pro.models.User;

import java.util.HashMap;
import java.util.Set;

public interface SelectContactActivityContract {
    interface SelectContactActivityView{

        void setScope(String scope);

        void setUID(String guid);

        void setContactAdapter(HashMap<String, User> userHashMap);
    }

    interface SelectContactActivityPresenter extends BasePresenter<SelectContactActivityView> {

        void getIntent(Intent intent);

        void getUserList(int i);

        void getFriendList(int i);

        void addContactToChat(String uid, SelectContactActivity selectContactActivity);//, Set<String> keySet);
        //void addContactToChat(String UniqueListenerID, String uid, SelectContactActivity selectContactActivity, Set<String> keySet);
    }
}
