package com.chatapp.v1.Contracts;

import android.content.Context;

import com.chatapp.v1.Base.BasePresenter;
import com.chatapp.v1.Base.BaseView;
import com.cometchat.pro.core.Call;
import com.cometchat.pro.core.CometChat;

import java.util.HashMap;
import java.util.List;

public interface CallsListContract {
    interface CallView extends BaseView{
        void setCallsAdapter(HashMap<String, Call> callList);

        void callviewCallback(Call call);
    }

    interface CallPresenter extends BasePresenter<CallView>
    {
        void initCallsView();
    }
}
