package com.chatapp.v1.Contracts;

import android.content.Context;

import com.chatapp.v1.Base.BasePresenter;
import com.cometchat.pro.core.Call;

import java.util.List;

public interface RecentCallsContract {
    interface RecentCallsView{
        void setRecentCallsAdapter(List<Call> callsList);

        void updateThreadCount(Call call);

        void setLastCall(Call call);

        void setFilterList(List<Call> hashMap);

        void refreshCalls();
    }

    interface RecentCallsPresenter extends BasePresenter<RecentCallsView>{
        void fetchCalls(Context context);

        void addCallListener(String presenceListener);

        void removeCallListener(String string);

//          void searchConversation(String s);

        void updateConversation();
    }
}
