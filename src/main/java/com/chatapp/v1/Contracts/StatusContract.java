package com.chatapp.v1.Contracts;

import android.content.Context;

import com.chatapp.v1.Base.BasePresenter;
import com.chatapp.v1.Base.BaseView;
import com.cometchat.pro.models.BaseMessage;
import com.cometchat.pro.models.User;

import java.util.List;

public interface StatusContract {
    interface StatusView extends BaseView {

        void setStatusAdapter(List<User> userList);

        void updateUnreadCount(User conversation);

        void setLastStatus(User user);

        void setFilterList(List<User> hashMap);

        void refreshStatus(BaseMessage message);
    }

    interface StatusPresenter extends BasePresenter<StatusContract.StatusView> {

        void fetchStatus(Context context);

        void addStatusListener(String presenceListener);

        void removeStatusListener(String string);

        //void searchConversation(String s);

        void updateStatus();
    }
}
