package com.chatapp.v1.Presenters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.chatapp.v1.Activity.ChatAppLoginActivity;
import com.cometchat.pro.core.BlockedUsersRequest;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.Group;
import com.cometchat.pro.models.MediaMessage;
import com.cometchat.pro.models.TextMessage;
import com.cometchat.pro.models.User;
import com.chatapp.v1.Activity.ChatAppActivity;
import com.chatapp.v1.Activity.LoginActivity;
import com.chatapp.v1.Base.Presenter;
import com.chatapp.v1.Contracts.CometChatActivityContract;
import com.chatapp.v1.Contracts.StringContract;
import com.chatapp.v1.Helper.PreferenceHelper;
import com.chatapp.v1.Utils.CommonUtils;
import com.cometchat.pro.constants.CometChatConstants;
import com.cometchat.pro.core.Call;
import com.cometchat.pro.core.CometChat;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class CometChatActivityPresenter extends Presenter<CometChatActivityContract.CometChatActivityView>
implements CometChatActivityContract.CometChatActivityPresenter {

    private static final String TAG = "CometChatActivityPresen";
    private Context context;

    @Override
    public void addMessageListener(final Context context, String listnerId) {
        this.context=context;

        CometChat.addMessageListener(listnerId, new CometChat.MessageListener() {

            @Override
            public void onTextMessageReceived(TextMessage message) {
                //Send push notification to user if logged in
            }

            @Override
            public void onMediaMessageReceived(MediaMessage message) {

            }
        });
    }

    @Override
    public void removeMessageListener(String listenerId) {
        CometChat.removeMessageListener(listenerId);
    }

    @Override
    public void addCallEventListener(Context context,String listenerId) {
        CometChat.addCallListener(listenerId, new CometChat.CallListener() {
            @Override
            public void onIncomingCallReceived(Call call) {
                //TODO: On Sunday Save this call instance in the database for records. Save call type as incoming_received

                Log.d(TAG, "onIncomingCallReceived: "+call.toString());

                if (call.getReceiverType().equals(CometChatConstants.RECEIVER_TYPE_USER)) {

                    CommonUtils.startCallIntent(context, (User) call.getCallInitiator(), call.getType(),
                            false, call.getSessionId());
                } else if (call.getReceiverType().equals(CometChatConstants.RECEIVER_TYPE_GROUP)) {

                    CommonUtils.startCallIntent(context, (Group) call.getCallReceiver(), call.getType(),
                            false, call.getSessionId());
                }
            }

            @Override
            public void onOutgoingCallAccepted(Call call) {
                //TODO: Save this call instance in the database for records  Save call type as outgoing_accepted

                Log.d(TAG, "onOutgoingCallAccepted: "+call.toString());
            }

            @Override
            public void onOutgoingCallRejected(Call call) {
                //TODO: Save this call instance in the database for records  Save call type as outgoing_rejected

                Log.d(TAG, "onOutgoingCallRejected: "+call.toString());
            }

            @Override
            public void onIncomingCallCancelled(Call call) {
                //TODO: Save this call instance in the database for records  Save call type as incoming_cancelled

                Log.d(TAG, "onIncomingCallCancelled: "+call.toString());
            }

        });
    }

    @Override
    public void removeCallEventListener(String tag) {
        CometChat.removeCallListener(tag);
    }

    @Override
    public void getBlockedUser(Context context) {

        PreferenceHelper.init(context);

        JSONObject jsonObject=new JSONObject();

        BlockedUsersRequest blockedUsersRequest = new BlockedUsersRequest.BlockedUsersRequestBuilder().setLimit(100).build();

        blockedUsersRequest.fetchNext(new CometChat.CallbackListener<List<User>>() {
            @Override
            public void onSuccess(List<User> users) {
                for(User user : users){
                    try {
                        jsonObject.put(user.getUid(),user.getUid());
                        //TODO: Save this blocked users instance in the database for records
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                PreferenceHelper.saveString(StringContract.PreferenceString.BLOCKED_USERS,jsonObject.toString());
            }

            @Override
            public void onError(CometChatException e) {
                Log.e(TAG, e.getMessage());
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void logOut(Context context) {
        CometChat.logout(new CometChat.CallbackListener<String>() {
            @Override
            public void onSuccess(String s) {
                //Intent intent=new Intent(context, LoginActivity.class);
                Intent intent=new Intent(context, ChatAppLoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                ((ChatAppActivity)context).finish();
            }

            @Override
            public void onError(CometChatException e) {
                Log.d(TAG, "onError: "+e.getMessage());
            }
        });
    }
}
