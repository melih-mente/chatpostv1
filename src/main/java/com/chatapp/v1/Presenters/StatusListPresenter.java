package com.chatapp.v1.Presenters;

import android.content.Context;

import com.chatapp.v1.Base.Presenter;
import com.chatapp.v1.Contracts.StatusContract;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.core.ConversationsRequest;
import com.cometchat.pro.core.UsersRequest;
import com.cometchat.pro.models.Conversation;
import com.cometchat.pro.models.TextMessage;

public class StatusListPresenter extends Presenter<StatusContract.StatusView> implements StatusContract.StatusPresenter {
    private UsersRequest usersRequest;
    private UsersRequest.UserStatus userStatus;
    private static final String TAG = "StatusListPresenter";
    private static final String FETCH_USER_STATUS = "https://admin.chat=post.com/api/getAppUsersStatus";

    //@Override
    public void fetchStatus(Context context) {
        //Use API to get STATUs from the API server

    }

    //@Override
    public void addStatusListener(String statusListener) {

    }

    //@Override
    public void removeStatusListener(String messageListener) {

    }

    //@Override
    public void updateStatus() {

    }
}
