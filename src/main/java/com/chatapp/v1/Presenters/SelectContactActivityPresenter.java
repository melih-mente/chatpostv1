package com.chatapp.v1.Presenters;

import android.content.Intent;
import android.util.Log;

import com.chatapp.v1.Activity.SelectContactActivity;
import com.chatapp.v1.Base.Presenter;
import com.chatapp.v1.Contracts.SelectContactActivityContract;
import com.chatapp.v1.Contracts.StringContract;
import com.chatapp.v1.Utils.Logger;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.core.UsersRequest;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class SelectContactActivityPresenter extends Presenter<SelectContactActivityContract.SelectContactActivityView> implements SelectContactActivityContract.SelectContactActivityPresenter{
    private UsersRequest usersRequest;
    HashMap<String, User> userHashMap=new HashMap<>();

    private static final String TAG = "SelectContactActPres";

    @Override
    public void getIntent(Intent intent){
        if(intent.hasExtra(StringContract.IntentStrings.INTENT_USER_ID)){
            getBaseView().setUID(intent.getStringExtra(StringContract.IntentStrings.INTENT_USER_ID));
        }

        if (intent.hasExtra(StringContract.IntentStrings.INTENT_SCOPE)){
            getBaseView().setScope(intent.getStringExtra(StringContract.IntentStrings.INTENT_SCOPE));
        }
    }

    @Override
    public void getUserList(int i) {
        if (usersRequest==null) {

            usersRequest  = new UsersRequest.UsersRequestBuilder().setLimit(i).build();

            usersRequest.fetchNext(new CometChat.CallbackListener<List<User>>() {
                @Override
                public void onSuccess(List<User> users) {
                    Logger.error(TAG," "+users.size());

                    for (int i = 0; i < users.size(); i++) {
                        userHashMap.put(users.get(i).getUid(), users.get(i));
                    }
                    getBaseView().setContactAdapter(userHashMap);

                }

                @Override
                public void onError(CometChatException e) {
                    Log.d(TAG, "fetchNext onError: ");
                }
            });
        }
        else {
            usersRequest.fetchNext(new CometChat.CallbackListener<List<User>>() {
                @Override
                public void onSuccess(List<User> users) {
                    if (users != null) {
                        Logger.error(TAG," "+users.size());

                        for (int i = 0; i < users.size(); i++) {
                            userHashMap.put(users.get(i).getUid(), users.get(i));
                        }
                        getBaseView().setContactAdapter(userHashMap);
                    }
                }

                @Override
                public void onError(CometChatException e) {
                    Log.d(TAG, "fetchNext old onError: ");
                }

            });
        }
    }

    @Override
    public void getFriendList(int i){}

    //String UniqueListenerID,
    @Override
    public void addContactToChat(String uid, SelectContactActivity selectContactActivity){
        //        CometChat.addUserListener(UniqueListenerID, new CometChat.UserListener() {
        //            @Override
        //            public void onUserOnline(User user) {
        //                //If is online,
        //            }
        //
        //            @Override
        //            public void onUserOffline(User user) {
        //                //If is offline,
        //            }
        //        });
    }
}
