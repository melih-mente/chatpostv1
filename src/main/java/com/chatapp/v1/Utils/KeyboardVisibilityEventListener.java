package com.chatapp.v1.Utils;

public interface KeyboardVisibilityEventListener {
    void onVisibilityChanged(boolean var1);
}