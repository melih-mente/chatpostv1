package com.chatapp.v1.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chatapp.v1.Activity.ChatAppActivity;
import com.chatapp.v1.R;
import com.cometchat.pro.constants.CometChatConstants;
import com.cometchat.pro.core.Call;
import com.cometchat.pro.models.Action;
import com.cometchat.pro.models.BaseMessage;
import com.cometchat.pro.models.Conversation;
import com.cometchat.pro.models.CustomMessage;
import com.cometchat.pro.models.Group;
import com.cometchat.pro.models.TextMessage;
import com.cometchat.pro.models.User;
import com.chatapp.v1.Activity.GroupChatActivity;
import com.chatapp.v1.Activity.OneToOneChatActivity;
import com.chatapp.v1.Contracts.StringContract;
import com.chatapp.v1.CustomView.CircleImageView;
import com.chatapp.v1.Utils.ColorUtils;
import com.chatapp.v1.Utils.FontUtils;
import com.chatapp.v1.Utils.MediaUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;

//TODO: The KINGDOM OF HOW THE LANDING PAGE LOOKS LIKE
public class RecentsListAdapter extends RecyclerView.Adapter<RecentsListAdapter.RecentsViewHolder> {

    private Toolbar toolbar;
    private final Context context;

    private List<Conversation> conversationList;

    private boolean isBlockedList;

    private int resId;

    private String guid,icon,groupName;

    private String uid,avatar,username;

    private HashMap<String, Integer> unreadCountMap;

    ActionMode mActionMode;

    private static final  String TAG = "RecentsListAdapter";

    private static String API_DEL_CONVERSATION = StringContract.AppDetails.API_DEL_CONVO;

    public RecentsListAdapter( List<Conversation> conversationList, Context context, int resId, boolean isBlockedList) {
        this.conversationList = conversationList;
        this.context = context;
        this.isBlockedList = isBlockedList;
        this.resId = resId;
        new FontUtils(context);
    }

    @NonNull
    @Override
    public RecentsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(resId, viewGroup, false);
        return new RecentsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentsViewHolder contactViewHolder, final int i) {

        Conversation conversation =conversationList.get(contactViewHolder.getAdapterPosition());
        Drawable statusDrawable;
        if (conversation.getConversationType().equals(CometChatConstants.CONVERSATION_TYPE_GROUP))
        {

            guid = ((Group)conversation.getConversationWith()).getGuid();
            groupName = ((Group)conversation.getConversationWith()).getName();
            contactViewHolder.userName.setText(groupName);
            if (((Group)conversation.getConversationWith()).getIcon() != null && !((Group)conversation.getConversationWith()).getIcon().isEmpty()) {
                icon = ((Group)conversation.getConversationWith()).getIcon();
                Glide.with(context).load(icon).into(contactViewHolder.avatar);
                contactViewHolder.avatar.setCircleBackgroundColor(context.getResources().getColor(android.R.color.white));
            }
            else {
                Drawable drawable = context.getResources().getDrawable(R.drawable.default_avatar);
                try {
                    contactViewHolder.avatar.setCircleBackgroundColor(ColorUtils.getMaterialColor(context));
                    contactViewHolder.avatar.setImageBitmap(MediaUtils.getPlaceholderImage(context, drawable));
                } catch (Exception e) {
                    contactViewHolder.avatar.setCircleBackgroundColor(context.getResources().getColor(R.color.secondaryDarkColor));
                    contactViewHolder.avatar.setImageDrawable(drawable);
                }
            }

        }
        else
        {
            uid = ((User)conversation.getConversationWith()).getUid();
            username = ((User)conversation.getConversationWith()).getName();
            contactViewHolder.userName.setText(username);
            if (((User)conversation.getConversationWith()).getAvatar() != null && !((User)conversation.getConversationWith()).getAvatar().isEmpty()) {
               avatar = ((User)conversation.getConversationWith()).getAvatar();
                Glide.with(context).load(avatar).into(contactViewHolder.avatar);
                contactViewHolder.avatar.setCircleBackgroundColor(context.getResources().getColor(android.R.color.white));
            } else {
                Drawable drawable = context.getResources().getDrawable(R.drawable.default_avatar);
                try {
                    contactViewHolder.avatar.setCircleBackgroundColor(ColorUtils.getMaterialColor(context));
                    contactViewHolder.avatar.setImageBitmap(MediaUtils.getPlaceholderImage(context, drawable));
                } catch (Exception e) {
                    contactViewHolder.avatar.setCircleBackgroundColor(context.getResources().getColor(R.color.secondaryDarkColor));
                    contactViewHolder.avatar.setImageDrawable(drawable);
                }
            }
        }

        contactViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (conversation.getConversationType().equals(CometChatConstants.CONVERSATION_TYPE_GROUP)) {
                    conversation.setUnreadMessageCount(0);
                    notifyDataSetChanged();
                    Intent intent = new Intent(context, GroupChatActivity.class);
                    intent.putExtra(StringContract.IntentStrings.INTENT_GROUP_ID, ((Group)conversation.getConversationWith()).getGuid());
                    intent.putExtra(StringContract.IntentStrings.INTENT_GROUP_NAME, ((Group)conversation.getConversationWith()).getName());
                    context.startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(context, OneToOneChatActivity.class);
                    intent.putExtra(StringContract.IntentStrings.USER_ID, ((User)conversation.getConversationWith()).getUid());
                    conversation.setUnreadMessageCount(0);
                    notifyDataSetChanged();
                    intent.putExtra(StringContract.IntentStrings.USER_AVATAR, ((User)conversation.getConversationWith()).getAvatar());
                    intent.putExtra(StringContract.IntentStrings.USER_NAME, ((User)conversation.getConversationWith()).getName());
                    context.startActivity(intent);
                }
            }
        });

        contactViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(context, "Delete this message", Toast.LENGTH_LONG).show();
                //Get conversation id
                String convoID = conversation.getConversationId();

                //Call a confirmation dialog with options
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Alert!!");
                alert.setMessage("Are you sure to delete this conversation?");
                
                //If user click yes, delete Call the delete message function using the id and close dialog
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do your work here
                        //Call the server url using the conversation ID
                        AsyncHttpClient client = new AsyncHttpClient();
                        RequestParams params = new RequestParams();

                        params.put("convoUID", convoID);
                        client.get(API_DEL_CONVERSATION, params, new TextHttpResponseHandler() {
                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                Log.d("AuthReqStatus", "Request fail! Status code: " + statusCode);
                                Log.d("AuthFailedResp", "Fail response: " + responseString);
                                Log.e("ERROR", throwable.toString());
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                Log.e(TAG, "Response params:" + responseString);
                                try {
                                    //DONE: Get Email and UID from the API touch point
                                    JSONObject respSTRING = new JSONObject(responseString);
                                    String success = respSTRING.getString("success");
                                    String message = respSTRING.getString("message");

                                    //On positive response, toggle the conversation
                                    //conversationList.remove(convoID);
                                    //notifyDataSetChanged();
                                    refreshData(conversationList);

                                    Toast.makeText(context, "Conversation successfully deleted", Toast.LENGTH_LONG).show();
                                    //Log.e(TAG, "Resp: 1. " + success + " 2. " + message + ".");
                                }
                                catch (JSONException ex) {
                                    //Log.e(TAG, "Error: " + ex.getMessage());
                                }
                            }
                        });

                        dialog.dismiss();
                    }
                });

                //If no, close the popup
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                alert.show();

                return true;
            }
        });



        //TODO: This is the place to add the minutes and other last seen messages icons
        contactViewHolder.userName.setTypeface(FontUtils.robotoRegular);
        contactViewHolder.lastMessage.setTypeface(FontUtils.robotoRegular);
        if (conversation.getLastMessage()!=null) {
            if (conversation.getLastMessage().getType().equals(CometChatConstants.MESSAGE_TYPE_TEXT)) {
                contactViewHolder.lastMessage.setText(((TextMessage) conversation.getLastMessage()).getText());
            } else if (conversation.getLastMessage().getType().equals(CometChatConstants.MESSAGE_TYPE_IMAGE)) {
                contactViewHolder.lastMessage.setText("Image File");
                //contactViewHolder.lastMessage.setDrawable
            } else if (conversation.getLastMessage().getType().equals(CometChatConstants.MESSAGE_TYPE_AUDIO)) {
                contactViewHolder.lastMessage.setText("Audio File");
            } else if (conversation.getLastMessage().getType().equals(CometChatConstants.MESSAGE_TYPE_VIDEO)) {
                contactViewHolder.lastMessage.setText("Video File");
            } else if (conversation.getLastMessage().getType().equals(CometChatConstants.MESSAGE_TYPE_CUSTOM)) {
                contactViewHolder.lastMessage.setText(((CustomMessage) conversation.getLastMessage()).getSubType());
            } else if (conversation.getLastMessage().getType().equals(CometChatConstants.MESSAGE_TYPE_FILE)) {
                contactViewHolder.lastMessage.setText("File");
            } else if (conversation.getLastMessage().getCategory().equals(CometChatConstants.CATEGORY_ACTION)) {
                contactViewHolder.lastMessage.setText(((Action) conversation.getLastMessage()).getMessage());
            } else if (conversation.getLastMessage().getCategory().equals(CometChatConstants.CATEGORY_CALL)) {
                contactViewHolder.lastMessage.setText("Call " + ((Call) conversation.getLastMessage()).getCallStatus());
            }
            else
            {
                contactViewHolder.lastMessage.setText("Custom Message");
            }
        }
        if (isBlockedList) {
            contactViewHolder.lastMessage.setVisibility(View.INVISIBLE);
        }
        if (conversation.getUnreadMessageCount()>0)
        {
            contactViewHolder.unreadCount.setVisibility(View.VISIBLE);
        }
        else
        {
            contactViewHolder.unreadCount.setVisibility(View.GONE);
        }
        contactViewHolder.view.setTag(R.string.message, conversation.getConversationId());
        contactViewHolder.unreadCount.setText(conversation.getUnreadMessageCount()+"");
    }


    public void refreshData(List<Conversation> conversationsList) {
        this.conversationList.clear();
        this.conversationList.addAll(conversationsList);
        notifyDataSetChanged();
    }

    public void setFilterList(List<Conversation> hashMap) {
        conversationList=hashMap;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
     return conversationList.size();
    }



    public void updateConversation(Conversation newConversation) {
        if (conversationList.contains(newConversation)){
            Conversation oldConversation=conversationList.get(conversationList.indexOf(newConversation));
            conversationList.remove(oldConversation);
            newConversation.setUnreadMessageCount(oldConversation.getUnreadMessageCount()+1);
            Log.e( "updateConversation: ",newConversation.toString());
            conversationList.add(0,newConversation);
        }
        notifyDataSetChanged();
    }

    public void deleteMessage(Conversation id) {
        //messageArrayList.remove(baseMessage.getId());
        conversationList.remove(id);
        notifyDataSetChanged();
    }


    public class RecentsViewHolder extends RecyclerView.ViewHolder {

        public TextView userName;
        public TextView lastMessage;
        public TextView unreadCount;
        public ImageView contextMsgIcon;
        public CircleImageView avatar;
        public View view;

        RecentsViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            avatar = view.findViewById(R.id.imageViewUserAvatar);
            userName = (TextView) view.findViewById(R.id.textviewUserName);
            lastMessage = (TextView) view.findViewById(R.id.textviewLastMessage);
            unreadCount = (TextView) view.findViewById(R.id.textviewSingleChatUnreadCount);
        }
    }
}
