package com.chatapp.v1.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chatapp.v1.R;
import com.cometchat.pro.core.Call;

import java.util.HashMap;

public class CallsListAdapter extends RecyclerView.Adapter<CallsListAdapter.CallsHolder> {
    private Context context;
    private HashMap<String, Call> callsList;

    public CallsListAdapter(HashMap<String, Call> callsList, Context context){
        this.callsList = callsList;
        this.context = context;
    }

    @NonNull
    @Override
    public CallsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.group_list_item, viewGroup, false);
        return new CallsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CallsHolder callsHolder, int i){

    }

    @Override
    public int getItemCount(){
        if(callsList != null){
            return callsList.size();
        }
        else {
            return 0;
        }
    }

    public class CallsHolder extends RecyclerView.ViewHolder{
        CallsHolder(@NonNull View itemView){
            super(itemView);
        }
    }
}
