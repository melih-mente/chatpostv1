package com.chatapp.v1.Helper;

import android.view.View;

public interface OnRecordClickListener {
    void onClick(View v);
}