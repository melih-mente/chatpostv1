package com.chatapp.v1.Helper;

public interface OnTopReachedListener {
    void onTopReached(int pos);
}
