package com.chatapp.v1.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import com.chatapp.v1.Presenters.LoginActivityPresenter;
import com.chatapp.v1.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.chatapp.v1.Adapter.AutoCompleteAdapter;
import com.chatapp.v1.Pojo.Option;
import com.chatapp.v1.Contracts.LoginActivityContract;
import com.chatapp.v1.Presenters.LoginAcitivityPresenter;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements LoginActivityContract.LoginActivityView {

    private TextInputLayout textInputLayoutUid;
    private TextInputEditText textInputEditTextUid;
    private TextInputEditText emailInput;
    private Button login;

    private LoginActivityContract.LoginActivityPresenter loginActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginActivityPresenter = new LoginActivityPresenter();
        loginActivityPresenter.attach(this);
        loginActivityPresenter.loginCheck();
        initComponentView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginActivityPresenter.detach();
    }

    private void initComponentView() {
        //textInputLayoutUid = findViewById(R.id.input_layout_uid);
        emailInput = findViewById(R.id.input_login_email);
        textInputEditTextUid = findViewById(R.id.guid);
        login = findViewById(R.id.btn_login);

        //AutoCompleteAdapter autoCompleteAdapter=new
               // AutoCompleteAdapter(this,R.layout.sample_user,getSampleUserList());

        //GridView gridView=findViewById(R.id.sample_list);

        //gridView.setAdapter(autoCompleteAdapter);

         //gridView.setOnItemClickListener((parent, view, position, id) -> {
         //    textInputEditTextUid.setText(getSampleUserList().get(position).getId());
          //   login();
         //});

        login.setOnClickListener(view -> login());
    }

    private void login(){
        String uid = textInputEditTextUid.getText().toString().trim();
        //String email = emailInput.getText().toString().trim();

        //TODO: Get the user email address here...

        if (!uid.isEmpty()) {
            Toast.makeText(LoginActivity.this, getString(R.string.wait), Toast.LENGTH_SHORT).show();
            loginActivityPresenter.Login(this, uid);
        }
        else {
            Toast.makeText(LoginActivity.this, getString(R.string.enter_uid_toast), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void startCometChatActivity() {

        startActivity(new Intent(LoginActivity.this, ChatAppActivity.class));
        finish();
    }
}
