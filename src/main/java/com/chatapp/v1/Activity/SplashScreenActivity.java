package com.chatapp.v1.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.chatapp.v1.R;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;

import java.util.Map;
import java.util.Set;

/**
 * Created by Tru247 on 21/12/2019.
 */

public class SplashScreenActivity extends Activity {
    private static String TAG = "SplashScreenAct";
    //public final int SPLASH_DISPLAY_LENGTH = 5000;
    public int SPLASH_DISPLAY_LENGTH;
    private String authToken;
    private String app_name = "ChatPost";
    Context context = this;

    private void initView(int time){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Start the app preference sensor here :: Read whether the user settings exist
                SharedPreferences sp = context.getSharedPreferences(app_name+"SP", 0);

                String uToken = sp.getString("user_token", null); // getting String
                authToken = uToken;
                //Toast.makeText(SplashScreenActivity.this, "Welcome to ChatPost™ - Connecting a billion people", Toast.LENGTH_LONG).show();
                //Toast.makeText(SplashScreenActivity.this, "UID: "+uToken, Toast.LENGTH_LONG).show();
                //Log.e(TAG, "uToken: "+uToken);

                if(uToken != null && !uToken.trim().isEmpty()) {
                    //Log.e(TAG, "uToken: "+uToken);
                    //Log into the API here again to confirm user's login activity using the token retrieved from Char server
                    CometChat.login(authToken, new CometChat.CallbackListener<User>() {
                        @Override
                        public void onSuccess(User user) {
                            //Log.e(TAG, "Login completed successfully for user: " + user.toString());
                            //Toast.makeText(context, "User token: "+uToken, Toast.LENGTH_LONG).show();
                            //DONE: If user token is available, run the ChatAppActivity
                            Intent i = new Intent(SplashScreenActivity.this, ChatAppActivity.class);
                            startActivity(i);
                        }
                        @Override
                        public void onError(CometChatException e) {
                            //Log.e(TAG, "Login failed with exception: " + e.getMessage());
                            Toast.makeText(context, "Login failed with exception: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
                else {
                    //Log.e(TAG, "No User Token!!");
                    //Toast.makeText(context, "No User Token!!", Toast.LENGTH_LONG).show();
                    //DONE: Else, run the ChatPostAuthActivity activity
                    Intent i = new Intent(SplashScreenActivity.this, ChatAppLoginActivity.class);
                    startActivity(i);
                }
            }
        }, time);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_layout);

        SPLASH_DISPLAY_LENGTH = 5000;
        initView(SPLASH_DISPLAY_LENGTH);
    }

    @Override
    protected void onResume(){
        super.onResume();

        SPLASH_DISPLAY_LENGTH = 15000;
        initView(SPLASH_DISPLAY_LENGTH);
    }

    @Override
    protected void onRestart(){
        super.onRestart();

        //SPLASH_DISPLAY_LENGTH = 15000;
        //initView(SPLASH_DISPLAY_LENGTH);
    }
}
