package com.chatapp.v1.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.chatapp.v1.Adapter.StatusAdapter;
import com.chatapp.v1.R;

public class CallListViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_list_view);

        //Adding data to the list
        // Array of strings...
        ListView simpleList;
        String callsList[] = {"Contact 1", "Contact 2", "Contact 3", "Contact 4", "Contact 5", "Contact 6"};
        int avatars[] = {
                R.drawable.default_avatar,
                R.drawable.default_black_avatar,
                R.drawable.default_avatar,
                R.drawable.default_black_avatar,
                R.drawable.default_avatar,
                R.drawable.default_black_avatar,
        };

        simpleList = (ListView) findViewById(R.id.simpleListView);
        StatusAdapter customAdapter = new StatusAdapter(getApplicationContext(), callsList, avatars);
        simpleList.setAdapter(customAdapter);
    }
}
