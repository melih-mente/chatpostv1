package com.chatapp.v1.Activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Camera;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.MotionEventCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;
import android.widget.Toast;

import com.chatapp.v1.Fragments.CallsFragment;
import com.chatapp.v1.Fragments.StatusFragment;
import com.chatapp.v1.Helper.CameraPreview;
import com.chatapp.v1.R;
import com.cometchat.pro.constants.CometChatConstants;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.chatapp.v1.Adapter.ViewPagerAdapter;
import com.chatapp.v1.Contracts.CometChatActivityContract;
import com.chatapp.v1.Contracts.StringContract;
import com.chatapp.v1.Fragments.ContactsFragment;
import com.chatapp.v1.Fragments.GroupListFragment;
import com.chatapp.v1.Fragments.RecentsFragment;
import com.chatapp.v1.Presenters.CometChatActivityPresenter;
import com.chatapp.v1.Utils.FontUtils;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ChatAppActivity extends AppCompatActivity implements CometChatActivityContract.CometChatActivityView { //ScrollHelper,

    private ActionMode mActionMode;
    private ViewPager mViewPager; //view pager
    private Toolbar toolbar;
    private AppBarLayout appBarLayout;
    private TabLayout tabs;
    private Context context;
    private Camera camera;
    //private FabIconAnimator fabIconAnimator;
    private ConstraintLayout container;
    private CoordinatorLayout mainContent;
    private CometChatActivityContract.CometChatActivityPresenter cometChatActivityPresenter;
    private ViewPagerAdapter adapter;
    private static final String TAG = "ChatAppActivity";
    public static HashMap<String ,Integer> countMap;
    private MenuItem searchItem;
    private ActionMode mAction;
    private SearchView searchView;
    private int pageNumber=0;

    private TextView editMessage;

    private String app_name = "ChatPost";
    FloatingActionButton fab;

    //Floating Action Button swift implementation
    int[] colorIntArray = {
            R.color.primaryColor,
            //R.color.primaryColor,
            R.color.primaryColor
    };
    int [] iconIntArray = {
            R.drawable.cc,
            //R.drawable.ic_fab_edit_24dp,
            //R.drawable.ic_new_call_24dp,
            R.drawable.ic_group_add_24dp

    };

    protected void animateFab(final int position){
        fab.clearAnimation();

        //Scale down animation
        ScaleAnimation shrink = new ScaleAnimation(1f, 0.2f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF);
        shrink.setDuration(150); //time in milliseconds
        shrink.setInterpolator(new DecelerateInterpolator());
        shrink.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //Change the FAB Color and icon
                fab.setBackgroundTintList(getResources().getColorStateList(colorIntArray[position]));
                fab.setImageDrawable(getResources().getDrawable(iconIntArray[position], null));

                //Scale up animation
                ScaleAnimation expand = new ScaleAnimation(0.2f, 1f, 0.2f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                expand.setDuration(100); //Time in milliseconds
                expand.setInterpolator(new AccelerateInterpolator());
                fab.startAnimation(expand);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fab.startAnimation(shrink);
    }

    protected void actOnFab(final int position){
        if (position == 0){
            //fab = findViewById(R.id.app_new_msg);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ChatAppActivity.this, SelectContactActivity.class);
                    startActivity(intent);
                }
            });
        }
        //        else if (position == 1){
        //            fab.setOnClickListener(new View.OnClickListener() {
        //                @Override
        //                public void onClick(View v) {
        //                    //TODO: Start new fragment with add new status
        //                    //Intent i = new Intent(ChatAppActivity.this, CallsListActivity.class); //TODO: This activity gets a list of logged in user contacts
        //                    //startActivity(i);
        //                    Snackbar.make(v, "New Call", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        //
        //                }
        //            });
        //        }
        else if (position == 1){
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i  = new Intent(ChatAppActivity.this, CreateGroupActivity.class);
                    startActivity(i);
                }
            });
        }
    }

    //    public int[] tabIcons = {
    //            R.drawable.ic_tab_chats,
    //            R.drawable.ic_tab_calls,
    //            R.drawable.ic_tab_groups
    //    };

    View rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_app);
        rootView = findViewById(R.id.main_content);

        context = this;
        cometChatActivityPresenter = new CometChatActivityPresenter();
        cometChatActivityPresenter.attach(this);
        initViewComponents();
    }

    private void initViewComponents() {

        //searchItem.setVisible(true);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        new FontUtils(this);

        appBarLayout = findViewById(R.id.appbar);

        //Drawable groupDrawable = getResources().getDrawable(R.drawable.ic_group_add_white_24dp);
        //Drawable newMsgDrawable = getResources().getDrawable(R.drawable.ic_chat);

        container = findViewById(R.id.constraint_container);
        mainContent = findViewById(R.id.main_content);

        new Thread(() -> cometChatActivityPresenter.getBlockedUser(ChatAppActivity.this)).start();

        tabs = findViewById(R.id.tabs);

        fab = findViewById(R.id.app_new_msg);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChatAppActivity.this, SelectContactActivity.class);
                startActivity(intent);
                //Snackbar.make(v, "New message", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });

        mViewPager = findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(2);

        setViewPager();
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                adapter.notifyDataSetChanged();
                pageNumber=i;
                if (i==0)
                {
                    searchItem.setVisible(true);
                }
                else
                {
                    searchItem.setVisible(true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        //setupTabIcons();

        //DONE: Add new chat Activity onclick New Message FAB. Display a list view with 1. New Group, 2. New Contact, 3. List of all Users
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                animateFab(tab.getPosition());
                actOnFab(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                animateFab(tab.getPosition());
                actOnFab(tab.getPosition());
            }
        });


        this.setChatAppTheme();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        cometChatActivityPresenter.addCallEventListener(context, TAG);
        Log.d(TAG, "onResume: ");
        cometChatActivityPresenter.addMessageListener(ChatAppActivity.this,TAG);

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
        cometChatActivityPresenter.removeMessageListener(TAG);
        cometChatActivityPresenter.removeCallEventListener(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //DONE: Get Calls and Status Fragment with data from the APP server
        //new StatusFragment().onActivityResult(requestCode, resultCode, data);
        //mViewPager.setCurrentItem(1);

        //new StatusFragment().onActivityResult(requestCode, resultCode, data);
        //mViewPager.setCurrentItem(2);

        new GroupListFragment().onActivityResult(requestCode, resultCode, data);
        mViewPager.setCurrentItem(1);

    }

    private void setViewPager() {

        //Drawable camChat = getResources().getDrawable(R.drawable.ic_camera_chat);
        //TODO: Create Camera & Recent calls fragment
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new RecentsFragment(), getString(R.string.chats));
        //adapter.addFragment(new StatusFragment(), getString(R.string.status));
        //adapter.addFragment(new CallsFragment(), getString(R.string.calls));
        adapter.addFragment(new GroupListFragment(), getString(R.string.groups));

        mViewPager.setAdapter(adapter);
        tabs.setupWithViewPager(mViewPager);

        this.setChatAppTheme();
    }

    private void setupTabIcons() {
        //tabs.getTabAt(0).setIcon(tabIcons[0]);
        //tabs.getTabAt(1).setIcon(tabIcons[1]);
        //tabs.getTabAt(2).setIcon(tabIcons[2]);
        //tabs.getTabAt(3).setIcon(tabIcons[3]);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        searchItem=menu.findItem(R.id.app_bar_search);
        searchItem.setVisible(false);
        SearchManager searchManager=((SearchManager)getSystemService(Context.SEARCH_SERVICE));

        if (searchItem!=null){

            searchView=((SearchView)searchItem.getActionView());
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    searchUser(s);
                    return false;
                }
            });

            searchView.setOnCloseListener(() -> {
                searchUser(null);
                return false;
            });
        }

        if (searchView!=null){
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }

        return  super.onCreateOptionsMenu(menu);
    }

    private void searchUser(String s) {

        switch (pageNumber){

            case 0:
                Log.e(TAG, "Chats search results");
                RecentsFragment recentsFragment = (RecentsFragment) adapter.getItem(0);
                recentsFragment.search(s);
                break;
            case 1:
                Log.e(TAG, "Calls search results");
                GroupListFragment groupListFragment = (GroupListFragment) adapter.getItem(2);
                groupListFragment.search(s);
                break;

        }
    }

    private void onShareClicked() {
        String appLink = StringContract.AppDetails.APP_URL;

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "Check out ChatPost, I use ChatPost to message and call the people I care about. Get it for free at "+appLink);

        startActivity(Intent.createChooser(intent, "Invite a friend via..."));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_settings:
                //Init the shared preferences
                SharedPreferences sett_prof = getApplicationContext().getSharedPreferences(app_name+"SP", 0); // 0 - for private mode
                SharedPreferences.Editor getEdit = sett_prof.edit();
                String userEmail = sett_prof.getString("user_email", null);
                String uToken = sett_prof.getString("user_token", null);
                String status = sett_prof.getString("user_status", null);
                getEdit.commit();

                User user = CometChat.getLoggedInUser();
                Intent intent = new Intent(context, UsersProfileViewActivity.class);

                intent.putExtra(StringContract.IntentStrings.PROFILE_VIEW, true);
                intent.putExtra(StringContract.IntentStrings.USER_ID, user.getUid());
                intent.putExtra(StringContract.IntentStrings.USER_AVATAR, user.getAvatar());
                intent.putExtra(StringContract.IntentStrings.USER_NAME, user.getName());

                //Get user token,  status and email address
                intent.putExtra(StringContract.IntentStrings.USER_EMAIL, userEmail);
                intent.putExtra(StringContract.IntentStrings.USER_TOKEN, uToken);
                intent.putExtra(StringContract.IntentStrings.USER_STATUS, status);
                startActivity(intent);
            break;

            case R.id.app_settings:
                Log.e(TAG, "User app settings");
                startActivity(new Intent(ChatAppActivity.this, SettingsActivity.class));
            break;

            case R.id.app_share:
                Log.e(TAG, "Let's invite them in!!");
                onShareClicked();
            break;

            case R.id.menu_log_out:
                //cometChatActivityPresenter.logOut(this);
                Log.e(TAG, "Change Account item clicked");
                //DONE: Remove the token. First from the server, then from the saved preferences...
                SharedPreferences pref2 = getApplicationContext().getSharedPreferences(app_name+"SP", 0); // 0 - for private mode
                SharedPreferences.Editor editor2 = pref2.edit();
                editor2.clear();
                editor2.apply(); // apply changes immediately in the background//editor.commit(); // commit changes immediately

                //Change the intent and activity right now to the Main Activity
                Intent i = new Intent(context, ChatAppLoginActivity.class);
                startActivity(i);
            break;
        }
        return super.onOptionsItemSelected(item);

    }

    private void setChatAppTheme() {
        //        rootView.setBackgroundResource(R.drawable.bg);
    }

    //Delete Message
    //boolean onTouchEvent(MotionEvent event)
    @Override
    public boolean onTouchEvent(MotionEvent event){
        int action = MotionEventCompat.getActionMasked(event);

        switch(action) {
            case (MotionEvent.ACTION_DOWN) :
                Log.d(TAG,"Action was DOWN");
                Toast.makeText(getApplicationContext(), "Action was DOWN", Toast.LENGTH_LONG).show();
                return true;
            case (MotionEvent.ACTION_MOVE) :
                Log.d(TAG,"Action was MOVE");
                Toast.makeText(getApplicationContext(), "Action was MOVE", Toast.LENGTH_LONG).show();
                return true;
            case (MotionEvent.ACTION_UP) :
                Log.d(TAG,"Action was UP");
                Toast.makeText(getApplicationContext(), "Action was UP", Toast.LENGTH_LONG).show();
                return true;
            case (MotionEvent.ACTION_CANCEL) :
                Log.d(TAG,"Action was CANCEL");
                Toast.makeText(getApplicationContext(), "Action was CANCEL", Toast.LENGTH_LONG).show();
                return true;
            case (MotionEvent.ACTION_OUTSIDE) :
                Log.d(TAG,"Movement occurred outside bounds of current screen element");
                Toast.makeText(getApplicationContext(), "Movement occurred outside bounds of current screen element", Toast.LENGTH_LONG).show();
                return true;
            default :
                return super.onTouchEvent(event);
        }
    }
}



