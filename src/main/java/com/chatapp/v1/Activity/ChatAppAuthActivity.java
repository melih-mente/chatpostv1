package com.chatapp.v1.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.auth0.android.jwt.JWT;
import com.chatapp.v1.R;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class ChatAppAuthActivity extends AppCompatActivity {

    private static final String TAG = "OTPAuthenticator";

    private String API_AUTH_URL = "https://admin.chat-post.com/api/authenticate";

    public Button mVerifyBtn;
    public EditText mOtpCode;
    public String otpCode;

    private String app_name = "ChatPost";

    private String authToken;

    //SessionManager session;

    //TODO: Change this server to use Parse
    //Parse the token from the server here
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_auth);

        final Context context = getApplicationContext();

        mVerifyBtn = findViewById(R.id.chatpost_auth_btn);
        mVerifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOtpCode = findViewById(R.id.chatpost_user_otp_code);
                otpCode = mOtpCode.getText().toString();
                Log.e(TAG, "OTP Code: " + otpCode);

                if (otpCode.matches("")){
                    Toast.makeText(context, "Please enter OTP Code", Toast.LENGTH_SHORT).show();
                }
                else {
                    AsyncHttpClient client = new AsyncHttpClient();
                    RequestParams params = new RequestParams();
                    params.put("otp", otpCode);
                    Log.e(TAG, "Request params: " +params);
                    client.get(API_AUTH_URL, params, new TextHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            Log.d("AuthReqStatus", "Request fail! Status code: " + statusCode);
                            Log.d("AuthFailedResp", "Fail response: " + responseString);
                            Log.e("ERROR", throwable.toString());
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String responseString) {
                            //DONE: To show the server response to the logged in user for validation and login
                            Log.e(TAG, "Response params:" + responseString);
                            try {
                                JSONObject chatapp = new JSONObject(responseString);

                                //DONE: Init the ChatApp
                                String usertoken = chatapp.getString("usertoken");
                                String uid = chatapp.getString("uid");
                                String email = chatapp.getString("email");
                                String name = chatapp.getString("name");
                                String status = chatapp.getString("status");
                                //String countrycode = chatapp.getString("cc"); //CANCELLED: Auto detect this on user login [Mobile app IP Geolocation]
                                int serverResp = chatapp.getInt("resp");
                                long ts = chatapp.getLong("timestamp");
                                String ops = chatapp.getString("ops");
                                String avatar = chatapp.getString("avatar");
                                String phone  = chatapp.getString("phone");
                                //String ac_status =  chatapp.getString("ac_status");
                                //String photo = chatapp.getString("photo");
                                //String wallpaper = chatapp.getString("wallpaper");
                                String upload_url = chatapp.getString("upload_url");
                                String download_url = chatapp.getString("download_url");
                                //Boolean result = chatapp.getBoolean("result");

                                if(serverResp == 1) {
                                    //DONE: Check if it is an onStart or onResume
                                    //Toast.makeText(context, "User ID: " + usertoken, Toast.LENGTH_SHORT).show();
                                    Toast.makeText(context, "Welcome to ChatPost", Toast.LENGTH_SHORT).show();

                                    //Done: Set Shared preferences for the application user for session management
                                    SharedPreferences pref = getApplicationContext().getSharedPreferences(app_name+"SP", 0); // 0 - for private mode
                                    SharedPreferences.Editor editor = pref.edit();

                                    //URLS
                                    editor.putString("upload_url", upload_url); //
                                    editor.putString("download_url", download_url);

                                    //Invite information
                                    editor.putString("invite_title", "https://chat-post.com");
                                    editor.putString("invite_subject", "Join ChatPost Messenger Today");
                                    editor.putString("invite_text", "Hey, I use ChatPost for free messaging, voice and video calls. Download it from <a href='https://play.google.com/store/apps/details?id=com.chat.post'>Google Play</a>");

                                    //Contacts
                                    //editor.putString("contact_name", "_from_server_");
                                    //editor.putString("contact_email", "_from_server_");
                                    //editor.putString("contact_gid", "_from_server_");
                                    //editor.putString("contact_ts", "_from_server_");
                                    //editor.putString("contact_status", "_from_server_");
                                    //editor.putString("contact_photo", "_from_server_");
                                    //editor.putString("contact_tn", "_from_server_");
                                    //editor.putString("contact_members", "_from_server_");

                                    //Response
                                    editor.putString("response_result", "_from_server_");
                                    editor.putString("response_op", ops);
                                    //editor.putString("response_error", "_from_server_");
                                    editor.putString("response_token", usertoken);
                                    //editor.putString("response_wallpaper", wallpaper);
                                    //editor.putString("response_contacts", "_from_server_"); //TODO: This is an array. Figure this shit out Tru247!!
                                    //editor.putString("response_name", username);
                                    //editor.putString("response_status", ac_status);
                                    //editor.putString("response_members", "_from_server_"); //TODO: This is an array. Figure this shit out Tru247!!
                                    //editor.putString("response_photo", photo);
                                    editor.putLong("response_ts", ts);

                                    editor.putString("user_uid", uid);  //DONE: Get from the server via custom API
                                    editor.putString("user_name", name);
                                    editor.putString("user_email", email);
                                    editor.putString("user_phone", phone);
                                    editor.putString("user_token", usertoken);
                                    editor.putString("user_status", status);
                                    editor.putString("user_avatar", avatar);

                                    //Server result
                                    //editor.putBoolean("result", result);

                                    editor.commit(); // commit changes

                                    //editor.apply(); // commit changes

                                    SharedPreferences prof = getApplicationContext().getSharedPreferences(app_name+"SP", 0); // 0 - for private mode
                                    SharedPreferences.Editor getEdit = prof.edit();
                                    String uToken = prof.getString("user_token", null);
                                    getEdit.commit();

                                    if(uToken != null && !uToken.trim().isEmpty()){
                                        authToken = uToken;
                                        //TODO: Create user object to hold logged in user details here to enable: user or group name, picture, status, presence information,
                                        CometChat.login(authToken, new CometChat.CallbackListener<User>() {
                                            @Override
                                            public void onSuccess(User user) {
                                                Log.e(TAG, "Login completed successfully for user: " + user.toString());
                                                Intent i = new Intent(ChatAppAuthActivity.this, ChatAppActivity.class);
                                                startActivity(i);
                                            }

                                            @Override
                                            public void onError(CometChatException e) {
                                                Log.d(TAG, "Login failed with exception: " + e.getMessage());
                                                Toast.makeText(context, "Oops! Login error!!", Toast.LENGTH_LONG).show();
                                            }
                                        });



                                        Intent intent = new Intent(ChatAppAuthActivity.this, ChatAppActivity.class);
                                        startActivity(intent);
                                    }
                                    else {
                                        Toast.makeText(context, "User ID is empty", Toast.LENGTH_SHORT).show(); //NB: This is a server error indicator
                                    }
                                }
                                else {
                                    Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                                }
                            }
                            catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }
}
