package com.chatapp.v1.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chatapp.v1.R;
import com.cometchat.pro.constants.CometChatConstants;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.models.MediaMessage;
import com.cometchat.pro.models.User;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.chatapp.v1.Adapter.MediaAdapter;
import com.chatapp.v1.Contracts.StringContract;
import com.chatapp.v1.Contracts.UserProfileViewActivityContract;
import com.chatapp.v1.Presenters.UserProfileViewPresenter;
import com.chatapp.v1.Utils.DateUtils;
import com.chatapp.v1.Utils.FontUtils;
import com.chatapp.v1.Utils.MediaUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;


import org.json.JSONObject;
import java.io.ByteArrayOutputStream;

import java.util.List;


import cz.msebera.android.httpclient.Header;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class UsersProfileViewActivity extends AppCompatActivity implements UserProfileViewActivityContract.UserProfileActivityView, View.OnClickListener {
    public static String TAG = "UserProfViewer";
    private Toolbar toolbar;

    private MediaAdapter mediaAdapter;

    private ImageView userAvatar, ivStatusIcon;

    private TextView tvStatus, tvVoiceCall, tvVideoCall;

    private Context context;

    private CollapsingToolbarLayout collapsingToolbar;

    private UserProfileViewActivityContract.UserProfileActivityPresenter userProfileActivityPresenter;

    private String contactUid;

    private User user = CometChat.getLoggedInUser();//TODO: Get this asynchronously to update the view immediately on loading;

    private RecyclerView rvMedia;

    private boolean isProfileView = false;

    private TextView tvUid;

    private String userStatus;

    private Drawable statusDrawable;
    private LinearLayoutManager linearLayoutManager;

    private String app_name = "ChatPost";

    EditText UserName, UserTel;
    TextView UserNameTwo;
    TextView UserEmail;
    Button updateUserProfileBtn;
    ImageButton ChooseBtn, UploadBtn;
    String AppUID, AppUserName, AppUserStatusMessage, AppUserPhone;

    View rootView;
    EmojiconEditText UserStatusMessage;
    ImageView emojiImageView;
    EmojIconActions emojIcon;

    private static final int CAMERA_REQUEST = 1888;
    private static final int GALLERY_REQUEST = 1889;

    //Spyware
    String SpyUserName, SpyUserStatus, SpyUserPhone;

    ProgressDialog prgDialog;
    String encodedString;
    RequestParams params = new RequestParams();
    String imgPath, fileName;
    Bitmap bitmap;
    private static int RESULT_LOAD_IMG = 1;
    private int PICK_IMAGE_REQUEST = 1;

    //uploadUserProfileImage
    private static String AVATAR_UPDATE_URL = "https://admin.chat-post.com/api/uploadUserProfileImage";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        rootView = findViewById(R.id.main_content);

        new FontUtils(this);
        context = this;
        userProfileActivityPresenter = new UserProfileViewPresenter();
        userProfileActivityPresenter.attach(this);

        //EmojiManager.install(new GoogleEmojiProvider());
        initViewComponent();
    }

    private void initViewComponent() {
        //sET PROGRESS BAR
        prgDialog = new ProgressDialog(this);
        // Set Cancelable as False
        prgDialog.setCancelable(false);

        //DONE: Init the SP here
        SharedPreferences spy = getApplicationContext().getSharedPreferences(app_name+"SP", 0); // 0 - for private mode
        SharedPreferences.Editor getEdit = spy.edit();
        SpyUserName = spy.getString("user_name", null);
        String uEmail = spy.getString("user_email", null);
        SpyUserPhone = spy.getString("user_phone", null);
        SpyUserStatus = spy.getString("user_status", null);

        String uAvatar = user.getAvatar();
        getEdit.commit();

        //DONE: Handle the user profile details dynamicity
        ImageView roundedimag = (ImageView) findViewById(R.id.roundedimag);
        // Load an image using Picasso library
        Picasso.with(getApplicationContext())
                .load(uAvatar)
                .into(roundedimag);
        //TODO:< Change user profile image here
        ChooseBtn = findViewById(R.id.user_avatar_upload_btn);
        ChooseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent();
                //intent.setType("image/*");
                //intent.setAction(Intent.ACTION_GET_CONTENT);
                //startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                loadImagefromGallery(view);
            }
        });

        UploadBtn = findViewById(R.id.user_avatar_upload_btn_axn);
        UploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Check permissions
                uploadImage(v);
            }
        });

        //TODO: Now get the view to display the above inputs for. Attach the above to the respective view IDs
        UserName = findViewById(R.id.user_name_edit_text);
        UserNameTwo = findViewById(R.id.user_name);
        UserEmail = findViewById(R.id.user_email_address);
        UserTel = findViewById(R.id.user_phone_edit_text);

        UserStatusMessage = (EmojiconEditText)findViewById(R.id.edit_user_status_text); //Emojification
        emojiImageView = (ImageView) findViewById(R.id.emoji_btn);
        emojIcon = new EmojIconActions(this, rootView, UserStatusMessage, emojiImageView);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e(TAG, "Keyboard opened!");
            }

            @Override
            public void onKeyboardClose() {
                Log.e(TAG, "Keyboard closed");
            }
        });

        JSONObject user_metadata = user.getMetadata();

        if(SpyUserName != null){
            UserName.setText(SpyUserName);
            UserNameTwo.setText(SpyUserName);
        }

        if(SpyUserStatus != null){
            UserStatusMessage.setText(SpyUserStatus);
        }

        if(uEmail != null){
            UserEmail.setText(uEmail);
        }

        if(SpyUserPhone != null){
            UserTel.setText(SpyUserPhone);
        }

        UserTel.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        //DONE: Update user profile onclick update button update_user_profile_button
        updateUserProfileBtn = findViewById(R.id.update_user_profile_button);
        updateUserProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "User metadata: "+user_metadata);
                //DONE: Now change the user name and status
                //DONE: Get user name input, status input. Both with emoji. User name must not be empty and not exceed 50, user status must be  150 characters

                //AppUID = user.getUid();
                AppUserName = UserName.getText().toString();
                AppUserStatusMessage = UserStatusMessage.getText().toString();
                AppUserPhone = UserTel.getText().toString();

                //TODO: Update user profile via the db
                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();

                params.put("user_email", UserEmail.getText().toString());
                params.put("user_name", AppUserName);
                params.put("user_status", AppUserStatusMessage);
                params.put("user_phone", AppUserPhone);
                Log.e(TAG, "User update params: "+params);

                client.post(StringContract.AppDetails.API_USER_UPDATE_URL, params, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Log.d(TAG, "Request fail! Status code: " + statusCode);
                        Log.d(TAG, "Fail response: " + responseString);
                        Log.e(TAG, throwable.toString());
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        //TODO: Set spy preference that spies on change and effects it immediately. Name, status, phone
                        SharedPreferences spy = getApplicationContext().getSharedPreferences(app_name+"SP", 0);
                        SharedPreferences.Editor editSpy = spy.edit();

                        editSpy.putString("user_phone", AppUserPhone);
                        editSpy.putString("user_status", AppUserStatusMessage);

                        editSpy.commit();


                        Toast.makeText(context, AppUserName + " profile updated successfully", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(10);

        TextView shared = findViewById(R.id.tvshared);
        TextView tvSeparator=findViewById(R.id.tvSeparator);
        userAvatar = findViewById(R.id.ivUserImage);
        ivStatusIcon = findViewById(R.id.imageViewProfileStatus);
        tvStatus = findViewById(R.id.textViewProfileStatusMessage);

        tvUid = findViewById(R.id.tvUid);
        rvMedia = findViewById(R.id.rv_media);

        tvVideoCall = findViewById(R.id.video_call);
        tvVoiceCall = findViewById(R.id.voice_call);
        //txtVwStatusUpdMsg = findViewById(R.id.emojiEditStatusText);

        tvVoiceCall.setOnClickListener(this);
        tvVideoCall.setOnClickListener(this);


        collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setContentScrimColor(getResources().getColor(R.color.primaryLightColor));
        collapsingToolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.primaryTextColor));
        collapsingToolbar.setExpandedTitleGravity(Gravity.START | Gravity.BOTTOM);
        collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.primaryLightColor));

        collapsingToolbar.setExpandedTitleTypeface(FontUtils.robotoRegular);
        collapsingToolbar.setCollapsedTitleTypeface(FontUtils.robotoMedium);

        tvVideoCall.setTypeface(FontUtils.robotoMedium);
        tvVoiceCall.setTypeface(FontUtils.robotoMedium);
        shared.setTypeface(FontUtils.robotoMedium);
        tvStatus.setTypeface(FontUtils.robotoRegular);
        tvUid.setTypeface(FontUtils.robotoRegular);

        userProfileActivityPresenter.handleIntent(getIntent());

        linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,true);
        rvMedia.setLayoutManager(linearLayoutManager);

        if (getIntent().hasExtra(StringContract.IntentStrings.PROFILE_VIEW)) {

            if (getIntent().getBooleanExtra(StringContract.IntentStrings.PROFILE_VIEW, false)) ;
            {
                isProfileView = true;
            }

        }

        //Let's Toggle with the visibility of user information
        LinearLayout profile_editor_layout = findViewById(R.id.profile_editor_layout);
        if (isProfileView) {
            tvVideoCall.setVisibility(View.GONE);
            tvVoiceCall.setVisibility(View.GONE);
            rvMedia.setVisibility(View.GONE);
            shared.setVisibility(View.GONE);
            tvSeparator.setVisibility(View.GONE);
            profile_editor_layout.setVisibility(LinearLayout.VISIBLE);
        }
        else {
            userProfileActivityPresenter.getMediaMessage(contactUid, 10);
            tvVideoCall.setVisibility(View.VISIBLE);
            tvVoiceCall.setVisibility(View.VISIBLE);
            rvMedia.setVisibility(View.VISIBLE);
            shared.setVisibility(View.VISIBLE);
            tvSeparator.setVisibility(View.VISIBLE);
            profile_editor_layout.setVisibility(LinearLayout.GONE);

        }


        if (isProfileView){
            //.setVisibility(LinearLayout.VISIBLE);
        }
        else {
            profile_editor_layout.setVisibility(LinearLayout.INVISIBLE);
        }

        rvMedia.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {

                if (!recyclerView.canScrollHorizontally(-1)) {
                    userProfileActivityPresenter.getMediaMessage(contactUid,10);
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        userProfileActivityPresenter.removeUserPresenceListener(getString(R.string.presenceListener));
        userProfileActivityPresenter.detach();

        // Dismiss the progress bar when application is closed
        if (prgDialog != null) {
            prgDialog.dismiss();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle(String name) {
        //STart Shared Preference and set the title using the stuff from the spyware
        collapsingToolbar.setTitle(name);
    }

    @Override
    public void setStatus(User user) {

          if (user!=null&&user.getUid().equals(contactUid)) {
              if (user.getStatus().equals(CometChatConstants.USER_STATUS_OFFLINE)) {
                  statusDrawable = context.getResources().getDrawable(R.drawable.cc_status_offline);
                  userStatus = DateUtils.getLastSeenDate(user.getLastActiveAt(), this);
              } else if (user.getStatus().equals(CometChatConstants.USER_STATUS_ONLINE)) {
                  statusDrawable = context.getResources().getDrawable(R.drawable.cc_status_available);
                  userStatus = user.getStatus();
              }
          }

        tvStatus.setText(userStatus);
        ivStatusIcon.setImageDrawable(statusDrawable);
    }

    @Override
    public void setUserImage(String avatar) {

        if (avatar != null) {

            userProfileActivityPresenter.setContactAvatar(context, avatar, userAvatar);

        } else {
            Drawable drawable = getResources().getDrawable(R.drawable.default_avatar);

            try {
                Bitmap bitmap = MediaUtils.getPlaceholderImage(this, drawable);
                userAvatar.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        userProfileActivityPresenter.addUserPresenceListener(getString(R.string.presenceListener));
    }

    @Override
    public void setUserId(String uid) {
        contactUid = uid;

        if (uid != null) {
            tvUid.setText("Uid:" + contactUid);
        }
    }

    @Override
    public void setAdapter(List<MediaMessage> messageList) {

        if (mediaAdapter == null) {
            mediaAdapter = new MediaAdapter(messageList, context);
            rvMedia.setAdapter(mediaAdapter);
        } else {
            if (messageList.size() != 0) {
                mediaAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.voice_call:
                userProfileActivityPresenter.sendCallRequest(this, contactUid, CometChatConstants.RECEIVER_TYPE_USER, CometChatConstants.CALL_TYPE_AUDIO);
                break;

            case R.id.video_call:
                userProfileActivityPresenter.sendCallRequest(this, contactUid, CometChatConstants.RECEIVER_TYPE_USER, CometChatConstants.CALL_TYPE_VIDEO);
                break;
        }
    }

    public void loadImagefromGallery(View view) {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }

    // When Image is selected from Gallery
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgPath = cursor.getString(columnIndex);
                cursor.close();
                ImageView imgView = (ImageView) findViewById(R.id.roundedimag);
                // Set the Image in ImageView
                imgView.setImageBitmap(BitmapFactory
                        .decodeFile(imgPath));
                // Get the Image's file name
                String fileNameSegments[] = imgPath.split("/");
                fileName = fileNameSegments[fileNameSegments.length - 1];
                // Put file name in Async Http Post Param which will used in Php web app

                user = CometChat.getLoggedInUser();
                String UID = user.getUid();
                params.put("uid", UID);
                params.put("filename", fileName);

            }
            else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }
    }

    // When Upload button is clicked
    public void uploadImage(View v) {
        // When Image is selected from Gallery
        if (imgPath != null && !imgPath.isEmpty()) {
            //prgDialog.setMessage("Converting Image to Binary Data");
            prgDialog.show();
            // Convert image to String using Base64
            encodeImageToString();
            // When Image is not selected from Gallery
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    "You must select image from gallery before you try to upload",
                    Toast.LENGTH_LONG).show();
        }
    }

    // AsyncTask - To convert Image to String
    public void encodeImageToString() {
        new AsyncTask<Void, Void, String>() {

            protected void onPreExecute() {

            };

            @Override
            protected String doInBackground(Void... params) {
                BitmapFactory.Options options = null;
                options = new BitmapFactory.Options();
                options.inSampleSize = 3;
                bitmap = BitmapFactory.decodeFile(imgPath,
                        options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                // Must compress the Image to reduce image size to make upload easy
                bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);
                byte[] byte_arr = stream.toByteArray();
                // Encode Image to String
                encodedString = Base64.encodeToString(byte_arr, 0);
                return "";
            }

            @Override
            protected void onPostExecute(String msg) {
                prgDialog.setMessage("Calling Upload");
                // Put converted Image string into Async Http Post param
                params.put("image", encodedString);
                // Trigger Image upload
                triggerImageUpload();
            }
        }.execute(null, null, null);
    }

    public void triggerImageUpload() {
        makeHTTPCall();
    }

    // Make Http call to upload Image to Php server
    public void makeHTTPCall() {
        prgDialog.setMessage("Invoking "+app_name+"™ Image Processor");
        AsyncHttpClient client = new AsyncHttpClient();
        // Don't forget to change the IP address to your LAN address. Port no as well.
        client.post(AVATAR_UPDATE_URL, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Hide Progress Dialog
                prgDialog.hide();
                Toast.makeText(getApplicationContext(), "Image updated successfully", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                prgDialog.hide();
                // When Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(),
                            "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(),
                            "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(
                            getApplicationContext(),
                            "Error Occurred n Most Common Error: n1. "+app_name+"™ App server is not running. HTTP Status code : "
                                    + statusCode, Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
    }
}
