package com.chatapp.v1.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.chatapp.v1.Adapter.StatusAdapter;
import com.chatapp.v1.R;


public class StatusListActivity extends AppCompatActivity {

    ListView stateList;
    String statusList[] = {"Love", "Fire", "Water", "Wind", "Stone", "Scissors"};
    int flags[] = {
            R.drawable.default_black_avatar,
            R.drawable.default_avatar,
            R.drawable.default_black_avatar,
            R.drawable.default_avatar,
            R.drawable.default_black_avatar,
            R.drawable.default_avatar,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_list);

        stateList = (ListView) findViewById(R.id.simpleListView);
        StatusAdapter customAdapter = new StatusAdapter(getApplicationContext(), statusList, flags);
        stateList.setAdapter(customAdapter);
    }
}
