package com.chatapp.v1.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.chatapp.v1.R;

public class ShowUserProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_user_profile);
    }
}
