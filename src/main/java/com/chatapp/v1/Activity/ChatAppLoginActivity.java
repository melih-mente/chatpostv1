package com.chatapp.v1.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.chatapp.v1.Contracts.LoginActivityContract;
import com.chatapp.v1.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;

public class ChatAppLoginActivity extends AppCompatActivity implements LoginActivityContract.LoginActivityView {
    private static final String TAG = "PwdLessLoginActivity";
    private String API_URL = "https://admin.chat-post.com/api";
    private LoginActivityContract.LoginActivityPresenter loginActivityPresenter;

    public Button mSendOTPBtn;
    public EditText mEmailField;
    public String cpEmail;

    private String app_name = "ChatPost";

    //START::Email validation
    public static boolean isMailValid(String email)
    {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
    //STOP::Email validation

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_login);

        final Context context = this;

        TextView view = (TextView)findViewById(R.id.terms_privacy);
        String disclaimer = "By clicking LOGIN, you agree to our <a href='https://chat-post.com/terms.php'>Terms</a>, <a href='https://chat-post.com/privacy.php'>Data Policy and Cookies Policy</a>. You may receive SMS Notifications from us and can opt out any time.";

        view.setText(Html.fromHtml(disclaimer));

        //DONE: Check if user preferences exist
        SharedPreferences sp = context.getSharedPreferences(app_name+"SP", 0);
        String uToken = sp.getString("user_token", null); // getting String
        if(uToken != null && !uToken.trim().isEmpty()){
            //Toast.makeText(context, "UID: "+uToken, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, ChatAppActivity.class);
            startActivity(intent);
        }
        //Toast.makeText(context, "No UID", Toast.LENGTH_SHORT).show();

        //Init Firebase
        //mAuth = FirebaseAuth.getInstance();

        mSendOTPBtn = findViewById(R.id.emailer_btn);
        mSendOTPBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEmailField = findViewById(R.id.chatpost_user_email);
                cpEmail = mEmailField.getText().toString();
                //Log.e(TAG, "Email input: " +cpEmail);
                if (cpEmail.matches("")){
                    Toast.makeText(context, "You did not enter an email", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (isMailValid(cpEmail)) {
                        Log.e(TAG, "Email input: " + cpEmail);

                        //Change The intent of activity from this class to the Auth Class
                        Intent intent = new Intent(context, ChatAppAuthActivity.class);
                        startActivity(intent);

                        AsyncHttpClient client = new AsyncHttpClient();
                        RequestParams params = new RequestParams();

                        params.put("email", cpEmail);
                        client.get(API_URL, params, new TextHttpResponseHandler() {
                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show();
                                //Log.d("AuthReqStatus", "Request fail! Status code: " + statusCode);
                                //Log.d("AuthFailedResp", "Fail response: " + responseString);
                                //Log.e("ERROR", throwable.toString());
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                //Log.e(TAG, "Response params:" + responseString);
                                try {
                                    //DONE: Get Email and UID from the API touch point
                                    JSONObject emailRespString = new JSONObject(responseString);
                                    String emailInfo = emailRespString.getString("resp");

                                    Toast.makeText(context, "Check "+emailInfo+" for One Time Password for login", Toast.LENGTH_LONG).show();
                                    //Log.e(TAG, "Resp: " + emailInfo);
                                }
                                catch (JSONException ex) {
                                    Log.e(TAG, "Error: " + ex.getMessage());
                                }
                            }
                        });
                    } else {
                        Toast.makeText(context, "Invalid email address!", Toast.LENGTH_SHORT).show();
                        //Log.e(TAG, "Invalid email address!!");
                    }
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume(){
        super.onResume();

        //Check whether the user has saved token in the app context.
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginActivityPresenter.detach();
    }

    @Override
    public void startCometChatActivity() {
        startActivity(new Intent(ChatAppLoginActivity.this, ChatAppActivity.class));
        finish();
    }
}
