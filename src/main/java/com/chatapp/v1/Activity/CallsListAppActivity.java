package com.chatapp.v1.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Camera;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;

import com.chatapp.v1.Adapter.ViewPagerAdapter;
import com.chatapp.v1.Fragments.CallsFragment;
import com.chatapp.v1.Fragments.GroupListFragment;
import com.chatapp.v1.Fragments.RecentsFragment;
import com.chatapp.v1.R;
import com.chatapp.v1.Utils.FontUtils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import java.util.HashMap;

public class CallsListAppActivity extends AppCompatActivity {
    private ViewPager mViewPager;
    private Toolbar toolbar;
    private AppBarLayout appBarLayout;
    private TabLayout tabs;
    private Context context;
    private Camera camera;
    //private FabIconAnimator fabIconAnimator;
    private ConstraintLayout container;
    private CoordinatorLayout mainContent;

    private ViewPagerAdapter adapter;
    private static final String TAG = "ChatAppActivity";
    public static HashMap<String ,Integer> countMap;
    private MenuItem searchItem;
    private SearchView searchView;
    private int pageNumber=0;

    private TextView editMessage;

    private String app_name = "ChatPost";

    View rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calls_list);
        rootView = findViewById(R.id.main_content);

        context = this;

    }

    private void initView(){

        //searchItem.setVisible(true);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        new FontUtils(this);


        //Drawable groupDrawable = getResources().getDrawable(R.drawable.ic_group_add_white_24dp);
        //Drawable newMsgDrawable = getResources().getDrawable(R.drawable.ic_chat);

        container = findViewById(R.id.constraint_container);
        mainContent = findViewById(R.id.main_content);

        //new Thread(() -> cometChatActivityPresenter.getBlockedUser(ChatAppActivity.this)).start();

        tabs = findViewById(R.id.tabs);

        //TODO: Add new chat Activity onclick New Message FAB
        FloatingActionButton fab = findViewById(R.id.app_new_msg);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Start new fragment with add new friend link on the top most line in the fragment
                Intent i = new Intent(CallsListAppActivity.this, SelectContactActivity.class); //TODO: This activity gets a list of logged in user contacts
                startActivity(i);
                Snackbar.make(v, "New message", Snackbar.LENGTH_LONG).setAction("Action", null).show();

            }
        });

        mViewPager = findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(2);

        setViewPager();
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                adapter.notifyDataSetChanged();
                pageNumber=i;
                if (i==0)
                {
                    searchItem.setVisible(true);
                }
                else
                {
                    searchItem.setVisible(true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }


    private void setViewPager() {

        //Drawable camChat = getResources().getDrawable(R.drawable.ic_camera_chat);
        //TODO: Create Camera & Recent calls fragment
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new RecentsFragment(), getString(R.string.chats));
        adapter.addFragment(new CallsFragment(), getString(R.string.calls));
        //adapter.addFragment(new StatusFragment(), getString(R.string.status));
        adapter.addFragment(new GroupListFragment(), getString(R.string.groups));

        mViewPager.setAdapter(adapter);
        tabs.setupWithViewPager(mViewPager);
        //setupTabIcons();

        //Adding data to the list
        // Array of strings...
        ListView simpleList;
        String statusList[] = {"I feel meehhhh", "God is Able", "Inshallah", "Baraka", "Germany", "NewZealand"};

        
    }
}
