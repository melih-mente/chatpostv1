package com.chatapp.v1.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chatapp.v1.Adapter.ContactListAdapter;
import com.chatapp.v1.Contracts.SelectContactActivityContract;
import com.chatapp.v1.Contracts.StringContract;
import com.chatapp.v1.Helper.RecyclerTouchListener;
import com.chatapp.v1.Presenters.SelectContactActivityPresenter;
import com.chatapp.v1.R;
import com.cometchat.pro.models.User;

import java.util.HashMap;
import java.util.Set;

public class SelectContactActivity extends AppCompatActivity implements SelectContactActivityContract.SelectContactActivityView {
    private static String TAG = "SelectContactAct";
    private Toolbar toolbar;
    private RecyclerView rvUserList;
    private LinearLayoutManager linearLayoutManager;
    private ContactListAdapter contactListAdapter;

    private SelectContactActivityContract.SelectContactActivityPresenter selectContactActivityPresenter;

    private String scope;

    private String uid;
    private String contactUid;
    private String avatar;

    private HashMap<String, View> selectedContactView = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_contact);

        selectContactActivityPresenter = new SelectContactActivityPresenter();
        selectContactActivityPresenter.attach(this);

        initView();
    }

    private void initView(){
        selectContactActivityPresenter.getIntent(getIntent());

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Select contact");

        rvUserList=findViewById(R.id.rvContactList);
        linearLayoutManager = new LinearLayoutManager(this);
        rvUserList.setLayoutManager(linearLayoutManager);

        selectContactActivityPresenter.getUserList(30);

        rvUserList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState){
                if(!recyclerView.canScrollVertically(1)){
                    selectContactActivityPresenter.getUserList(30);
                }
            }
        });

        rvUserList.addOnItemTouchListener(new RecyclerTouchListener(this,
                rvUserList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View var1, int position) {
                User user= (User) var1.getTag(R.string.user);
                Log.e(TAG, "Selected user id: "+user.getUid());
                contactUid = user.getUid();
                avatar = user.getAvatar();

                //TODO: Create a logic to handle the select contact for group and single message contact
                Intent intent = new Intent(getApplicationContext(), OneToOneChatActivity.class);
                intent.putExtra(StringContract.IntentStrings.USER_ID, user.getUid());
                intent.putExtra(StringContract.IntentStrings.USER_AVATAR, user.getAvatar());
                intent.putExtra(StringContract.IntentStrings.USER_NAME, user.getName());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View var1, int position) {

            }
        }));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setScope(String scope) {
        this.scope=scope;
    }

    @Override
    public void setUID(String uid) {
        this.uid=uid;
    }

    @Override
    public void setContactAdapter(HashMap<String, User> userHashMap) {

        if (contactListAdapter == null) {
            contactListAdapter = new ContactListAdapter(userHashMap, this, R.layout.contact_list_item,true);
            rvUserList.setAdapter(contactListAdapter);
        }
        else {
            if (userHashMap != null && userHashMap.size() == 0) {
                contactListAdapter.refreshData(userHashMap);
            }
        }

    }
}
