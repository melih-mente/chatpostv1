package com.chatapp.v1.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;

import com.chatapp.v1.R;

public class UsersStatusActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_status);

        Context context = this;

        //RecyclerView rv = (RecyclerView)findViewById(R.id.rv);
        RecyclerView rv = (RecyclerView)findViewById(R.id.userStatusCV);
        rv.setHasFixedSize(true);

        //make your RecyclerView look like a ListView.
        LinearLayoutManager llm = new LinearLayoutManager(context);
        rv.setLayoutManager(llm);


    }
}
