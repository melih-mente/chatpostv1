package com.chatapp.v1.Activity;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import androidx.appcompat.app.AppCompatActivity;

import com.chatapp.v1.Adapter.RecentsListAdapter;
import com.chatapp.v1.Contracts.RecentsContract;
import com.cometchat.pro.models.BaseMessage;
import com.cometchat.pro.models.Conversation;

import java.util.List;

public class RecentsListActivity extends AppCompatActivity implements RecentsContract.RecentsView, ActionMode.Callback {

    private RecentsListAdapter conversationAdapter;

    /**
     * Constructor
     */
    RecentsListActivity(){

    }

    @Override
    public void setTitle(String string){

    }

    @Override
    public void setRecentAdapter(List<Conversation> conversationList){

    }

    @Override
    public void updateUnreadCount(Conversation conversation){

    }

    @Override
    public void setLastMessage(Conversation conversation){

    }

    @Override
    public void setFilterList(List<Conversation> hashMap){

    }

    @Override
    public void refreshConversation(BaseMessage message){

    }

    @Override
    public void setDeleteConversation(Conversation conversation){
        if(conversationAdapter != null){
            conversationAdapter.deleteMessage(conversation);
        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu){
        return false;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu){
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item){
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode){

    }
}
