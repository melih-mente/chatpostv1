package com.chatapp.v1.Base;

public class Status {

    private String statusMessage, avatarUrl, attachmentUrl, UID, createdAt;
    private int visibility;

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getAttachmentUrl() {
        return attachmentUrl;
    }

    public void setAttachmentUrl(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String uid) {
        this.attachmentUrl = uid;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String time) {
        this.createdAt = time;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int i) {
        this.visibility = i;
    }

    public Status(String state, String attachmentLink, String uid, String time, int visible){
        this.statusMessage = state;
        this.attachmentUrl = attachmentLink;
        this.UID = uid;
        this.createdAt = time;
        this.visibility = visible;
    }
}
