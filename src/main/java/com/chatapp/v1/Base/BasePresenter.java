package com.chatapp.v1.Base;

public interface BasePresenter<V> {

    void attach(V baseView);

    void detach();
}
