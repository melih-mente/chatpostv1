package com.chatapp.v1;

import android.app.Application;
import android.os.StrictMode;
import android.widget.Toast;
import com.cometchat.pro.core.AppSettings;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.chatapp.v1.Contracts.StringContract;

import timber.log.Timber;


public class ChatApplication extends Application {

    private static final String TAG = "ChatApplication";

    @Override
    public void onCreate() {
        super.onCreate();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        //Logger.enableLogs("4ddd5d736cf33ca31a0b4c72ae64b6d5");
        AppSettings appSettings=new AppSettings.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(StringContract.AppDetails.REGION).build();
        CometChat.init(this, StringContract.AppDetails.APP_ID,appSettings,new CometChat.CallbackListener<String>() {

            @Override
            public void onSuccess(String s) {
                //Toast.makeText(ChatApplication.this, "Welcome to ChatPost™ - Connecting a billion people", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(CometChatException e) {
                Toast.makeText(ChatApplication.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                Timber.d("onError: %s", e.getMessage());
            }

        });
    }
}

