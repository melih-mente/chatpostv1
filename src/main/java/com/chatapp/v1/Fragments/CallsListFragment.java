package com.chatapp.v1.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chatapp.v1.Adapter.CallsListAdapter;
import com.chatapp.v1.Contracts.CallsListContract;
import com.chatapp.v1.Helper.ScrollHelper;
import com.chatapp.v1.R;
import com.cometchat.pro.core.Call;

import java.util.HashMap;

public class CallsListFragment extends Fragment {
        //implements CallsListContract.CallView{
    private RecyclerView callsRecyclerView;

    private CallsListAdapter callsListAdapter;
    private CallsListContract.CallPresenter callPresenter;
    private com.cometchat.pro.core.Call calls;

    private ScrollHelper scrollHelper;
    private LinearLayoutManager linearLayoutManager;

    private Call noCalls;

    public CallsListFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_calls, container, false);

        initViewComponent(view);



        return view;
    }

    private void initViewComponent(View view){

    }

    //@Override
    public void setCallsListAdapter(HashMap<String, com.cometchat.pro.core.Call> callsList){
        if (callsListAdapter == null){
            callsListAdapter = new CallsListAdapter(callsList, getActivity());
            callsRecyclerView.setAdapter(callsListAdapter);
        }
        if(callsList.size() == 0){
            //callsList.setVisibility(View.VISIBLE);
        }
        else{
            //callsList.setVisibility(View.GONE);
        }
    }
}
