package com.chatapp.v1.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chatapp.v1.Contracts.StatusContract;
import com.chatapp.v1.Helper.ScrollHelper;
import com.chatapp.v1.Presenters.StatusListPresenter;
import com.chatapp.v1.R;
import com.cometchat.pro.models.User;
import com.facebook.shimmer.ShimmerFrameLayout;

public class CallsFragment extends Fragment {
    private RecyclerView statusRecyclerView;

    private ScrollHelper scrollHelper;
    private LinearLayoutManager linearLayoutManager;
    private User user;

    private ShimmerFrameLayout recentShimmer;

    private ImageView ivNoCalls;

    private TextView tvNoCalls;

    private StatusContract.StatusPresenter statusPresenter;

    public CallsFragment(){
        //Constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState){
        //Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_calls, container, false);

        ivNoCalls = view.findViewById(R.id.ivNoCalls);
        tvNoCalls=view.findViewById(R.id.tvNoCalls);
        statusRecyclerView = view.findViewById(R.id.calls_recycler_view);
        recentShimmer=view.findViewById(R.id.status_shimmer);

        linearLayoutManager=new LinearLayoutManager(getContext());
        statusRecyclerView.setLayoutManager(linearLayoutManager);
        statusRecyclerView.setItemAnimator(new DefaultItemAnimator());

        //statusPresenter = new RecentsListPresenter();
        statusPresenter = new StatusListPresenter();

        //statusPresenter.attach(this);
        //statusPresenter.attach(this);
        //statusPresenter.attach(this);

        //new Thread(() -> recentPresenter.fetchConversations(getContext())).start();
        new Thread(() -> statusPresenter.fetchStatus(getContext())).start();

        statusRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (!recyclerView.canScrollVertically(1)) {
                    statusPresenter.fetchStatus(getContext());
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        return view;
    }
}
