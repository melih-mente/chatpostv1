package com.chatapp.v1.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chatapp.v1.Contracts.StringContract;
import com.chatapp.v1.R;
import com.cometchat.pro.helpers.CometChatHelper;
import com.cometchat.pro.models.BaseMessage;
import com.cometchat.pro.models.Conversation;
import com.cometchat.pro.models.User;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.chatapp.v1.Adapter.RecentsListAdapter;
import com.chatapp.v1.Contracts.RecentsContract;
import com.chatapp.v1.Helper.ScrollHelper;
import com.chatapp.v1.Presenters.RecentsListPresenter;

import java.util.ArrayList;
import java.util.List;

import static org.webrtc.ContextUtils.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */

public class RecentsFragment extends Fragment implements RecentsContract.RecentsView{

    private RecentsListAdapter recentsListAdapter;

    private RecyclerView recentsRecyclerView;

    private ImageView ivNoRecents;

    private TextView tvNoRecents;

    private RecentsContract.RecentsPresenter recentPresenter;

    private ScrollHelper scrollHelper;

    private User user;

    private ShimmerFrameLayout recentShimmer;

    private LinearLayoutManager linearLayoutManager;
    private List<Conversation> conversationList=new ArrayList<>();

    public RecentsFragment() {
        // Required empty public constructor
    }

    @Override
    public void setTitle(String title) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_recents, container, false);

        ivNoRecents = view.findViewById(R.id.ivNoRecents);
        tvNoRecents=view.findViewById(R.id.tvNoRecents);
        recentsRecyclerView = view.findViewById(R.id.recents_recycler_view);
        recentShimmer=view.findViewById(R.id.recent_shimmer);

        linearLayoutManager=new LinearLayoutManager(getContext());
        recentsRecyclerView.setLayoutManager(linearLayoutManager);
        recentsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        recentPresenter = new RecentsListPresenter();

        recentPresenter.attach(this);

        new Thread(() -> recentPresenter.fetchConversations(getContext())).start();

        recentsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (!recyclerView.canScrollVertically(1)) {
                    recentPresenter.fetchConversations(getContext());
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        //        recentsRecyclerView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
        //            @Override
        //            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {
        //                Toast.makeText(getApplicationContext(), "long clicked", Toast.LENGTH_SHORT).show();
        //                return true;
        //            }
        //        });

        recentsRecyclerView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getApplicationContext(), "Looong clicked on the message item", Toast.LENGTH_LONG).show();
                return false;
            }
        });

        return view;
    }

    //Implement item click and long click over list view
    private void implementListViewClickListeners() {
        recentsRecyclerView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });
    }


    @Override
    public void setFilterList(List<Conversation> hashMap) {
        recentsListAdapter.setFilterList(hashMap);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        recentPresenter.removeMessageListener(getString(R.string.presenceListener));
    }

    @Override
    public void onStart() {
        super.onStart();
        recentPresenter.addMessageListener(getString(R.string.presenceListener));
    }

    @Override
    public void onResume() {
        super.onResume();
        recentPresenter.addMessageListener(getString(R.string.presenceListener));
        recentPresenter.fetchConversations(getContext());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //scrollHelper = (ScrollHelper) context;

    }

    @Override
    public void refreshConversation(BaseMessage message) {
        Conversation newConversation= CometChatHelper.getConversationFromMessage(message);
         recentsListAdapter.updateConversation(newConversation);
    }

    @Override
    public void setRecentAdapter(List<Conversation> conversationList) {
        this.conversationList.addAll(conversationList);
        Log.e( "setRecentAdapter: ", conversationList.toString() );
        if (recentsListAdapter == null) {
            recentsListAdapter = new RecentsListAdapter(conversationList, getContext(), R.layout.recent_list_item,false);
            recentsRecyclerView.setAdapter(recentsListAdapter);
            recentShimmer.stopShimmer();
            recentShimmer.setVisibility(View.GONE);

        } else {
            if (this.conversationList != null) {
                recentsListAdapter.refreshData(conversationList);
            }
        }

        if (this.conversationList != null && this.conversationList.size() == 0) {
            tvNoRecents.setVisibility(View.VISIBLE);
            ivNoRecents.setVisibility(View.VISIBLE);
        } else {
            tvNoRecents.setVisibility(View.GONE);
            ivNoRecents.setVisibility(View.GONE);
        }

//        recentsListAdapter.on
    }

    @Override
    public void updateUnreadCount(Conversation conversation) {

    }

    @Override
    public void setLastMessage(Conversation conversation) {

    }


    public void search(String s) {
        //recentPresenter.searchConversation(s);
        //recentPresenter.addMessageListener(s);
    }

    @Override
    public void setDeleteConversation(Conversation conversation){

    }

}
