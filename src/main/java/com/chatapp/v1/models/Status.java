package com.chatapp.v1.models;

//public class Status {
//}

import com.chatapp.v1.R;

import java.util.ArrayList;
import java.util.List;

class Status {
    String name;
    String statusMsg;
    int photoId;

    Status(String nm, String msg, int imgId) {
        this.name = nm;
        this.statusMsg = msg;
        this.photoId = imgId;
    }

    //private List<Status> persons;
    private List<Status> statusList;

    // This method creates an ArrayList that has three Person objects
    // Checkout the project associated with this tutorial on Github if
    // you want to use the same images.
    private void initializeData(){
        statusList = new ArrayList<>();
        statusList.add(new Status("Tru247", "Creazone Di Adama...", R.drawable.default_black_avatar));
        statusList.add(new Status("Johnte", "Mthi wa mucaga...", R.drawable.default_avatar));
        statusList.add(new Status("Mumus", "Eti what Di Adama...", R.drawable.default_black_avatar));
        statusList.add(new Status("Bobs", "Long live rastaman...", R.drawable.default_avatar));
    }
}